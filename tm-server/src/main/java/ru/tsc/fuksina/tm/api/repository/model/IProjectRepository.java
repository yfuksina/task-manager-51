package ru.tsc.fuksina.tm.api.repository.model;

import ru.tsc.fuksina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
