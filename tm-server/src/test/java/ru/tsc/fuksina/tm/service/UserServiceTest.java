package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.marker.DatabaseCategory;
import ru.tsc.fuksina.tm.service.dto.UserServiceDTO;
import ru.tsc.fuksina.tm.util.HashUtil;

import java.util.List;

@Category(DatabaseCategory.class)
public class UserServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private IUserServiceDTO userService;

    private static long INITIAL_SIZE;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @Before
    public void init() {
        userService = new UserServiceDTO(propertyService, connectionService);
        userService.create("user_1", "pass_1", Role.USUAL);
        userService.create("user_2", "pass_2", Role.ADMIN);
        userService.create("user_3", "pass_3", "email_3");
        INITIAL_SIZE = userService.getSize();
    }

    @After
    public void end() {
        userService.clear();
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", ""));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        @Nullable final UserDTO user = userService.create("user_4", "pass_4");
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass", "email"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", "", "email"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create("log", "pass", ""));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        Assert.assertThrows(EmailExistsException.class,
                () -> userService.create("user_4", "pass", "email_3")
        );
        @Nullable final UserDTO user = userService.create("user_4", "pass_4", "email_4");
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertEquals("email_4", user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass", Role.USUAL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", "", Role.USUAL));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        @Nullable final UserDTO user = userService.create("user_4", "pass_4", Role.ADMIN);
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findAll() {
        @NotNull final List<UserDTO> users = userService.findAll();
        Assert.assertEquals(INITIAL_SIZE, users.size());
    }

    @Test
    public void add() {
        @Nullable UserDTO user = new UserDTO();
        userService.add(user);
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
    }

    @Test
    public void remove() {
        @NotNull final UserDTO user = userService.create("user_4", "pass");
        userService.remove(user);
        Assert.assertEquals(INITIAL_SIZE, userService.getSize());
        Assert.assertNull(userService.findOneById(user.getId()));
    }

    @Test
    public void clear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void findOneByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findOneByLogin(""));
        @NotNull String login = "login";
        userService.create(login, "pass");
        Assert.assertNotNull(userService.findOneByLogin(login));
        Assert.assertEquals(login, userService.findOneByLogin(login).getLogin());
    }

    @Test
    public void findOneByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findOneByEmail(""));
        @NotNull String email = "e-mail";
        userService.create("login", "pass", email);
        Assert.assertNotNull(userService.findOneByEmail(email));
        Assert.assertEquals(email, userService.findOneByEmail(email).getEmail());
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertTrue(userService.isLoginExists("user_1"));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(userService.isEmailExists(""));
        Assert.assertTrue(userService.isEmailExists("email_3"));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        @NotNull final String login = "user_1";
        userService.removeByLogin(login);
        Assert.assertNull(userService.findOneByLogin(login));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(AccessDeniedException.class, () -> userService.setPassword("", "new_pass"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", ""));
        Assert.assertNull(userService.setPassword("123", "new_pass"));
        @Nullable final UserDTO user = userService.create("logg", "pass");
        @NotNull final String pass = "new_pass";
        @Nullable final String hash = HashUtil.salt(propertyService, pass);
        userService.setPassword(user.getId(), pass);
        Assert.assertEquals(hash, userService.findOneByLogin("logg").getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(AccessDeniedException.class,
                () -> userService.updateUser("", "first", "last", "middle")
        );
        Assert.assertNull(userService.updateUser("123", "first", "last", "middle"));
        @NotNull final String id = userService.findOneByLogin("user_1").getId();
        @NotNull final UserDTO user = userService.updateUser(id, "first", "last", "middle");
        Assert.assertEquals("first", user.getFirstName());
        Assert.assertEquals("last", user.getLastName());
        Assert.assertEquals("middle", user.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        userService.lockUserByLogin("user_1");
        @NotNull final UserDTO user = userService.findOneByLogin("user_1");
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        userService.lockUserByLogin("user_1");
        userService.unlockUserByLogin("user_1");
        @NotNull final UserDTO user = userService.findOneByLogin("user_1");
        Assert.assertEquals(false, user.getLocked());
    }

}
