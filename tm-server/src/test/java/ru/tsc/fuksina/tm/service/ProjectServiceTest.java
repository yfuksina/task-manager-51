package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.fuksina.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.fuksina.tm.dto.model.ProjectDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.marker.DatabaseCategory;
import ru.tsc.fuksina.tm.service.dto.ProjectServiceDTO;
import ru.tsc.fuksina.tm.service.dto.UserServiceDTO;
import ru.tsc.fuksina.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

@Category(DatabaseCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private static String USER_ID;

    private static long INITIAL_SIZE;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @Before
    public void init() {
        projectService = new ProjectServiceDTO(connectionService);
        userService = new UserServiceDTO(propertyService, connectionService);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        projectService.create(USER_ID, "project_1", "proj_1");
        INITIAL_SIZE = projectService.getSize();
    }

    @After
    public void end() {
        projectService.clear();
        userService.clear();
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project_2"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, ""));
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project_2");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getName());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project_2", "proj_2"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, "", "proj_2"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(USER_ID, "project_2", ""));
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project_2", "proj_2");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getDescription());
    }

    @Test
    public void createWithDescriptionAndDate() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project_2", "proj_2"));
        @NotNull final ProjectDTO project = projectService.create(
                USER_ID,
                "project_2",
                "proj_2",
                DateUtil.toDate("01.01.2021"),
                DateUtil.toDate("01.10.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getDateStart());
        Assert.assertNotNull(project.getDateEnd());
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(""));
        @NotNull final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertEquals(INITIAL_SIZE, projects.size());
        @NotNull final List<ProjectDTO> userProjects = projectService.findAll(USER_ID);
        Assert.assertEquals(1, userProjects.size());
        @NotNull final List<ProjectDTO> newUserProjects = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, newUserProjects.size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID, ""));
        @NotNull final String projectName = "proj find by id";
        @NotNull final ProjectDTO project = projectService.create(USER_ID, projectName);
        @NotNull final String id = project.getId();
        Assert.assertNotNull(projectService.findOneById(id));
        Assert.assertEquals(projectName, projectService.findOneById(id).getName());
        Assert.assertNotNull(projectService.findOneById(USER_ID, id));
        Assert.assertEquals(projectName, projectService.findOneById(USER_ID, id).getName());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove("", new ProjectDTO()));
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project remove");
        projectService.remove(USER_ID, project);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID, ""));
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project remove");
        @NotNull final String id = project.getId();
        projectService.removeById(USER_ID, id);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        Assert.assertNull(projectService.findOneById(id));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(""));
        @NotNull final String userId = userService.create("user_2", "user").getId();
        projectService.create(userId, "project");
        projectService.clear(USER_ID);
        Assert.assertEquals(0, projectService.getSize(USER_ID));
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID, ""));
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project exists");
        Assert.assertTrue(projectService.existsById(USER_ID, project.getId()));
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.updateById("", "123", "new", "new descr")
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> projectService.updateById(USER_ID, "", "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> projectService.updateById(USER_ID, "123", "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> projectService.updateById(USER_ID, "123", "new", "")
        );
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final String id = projectService.create(USER_ID, "old name").getId();
        @NotNull final ProjectDTO project = projectService.updateById(USER_ID, id, name, description);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.changeProjectStatusById("", "123", Status.IN_PROGRESS)
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> projectService.changeProjectStatusById(USER_ID, "", Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> projectService.changeProjectStatusById(USER_ID, "123", Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final String id = projectService.create(USER_ID, "project change status").getId();
        @NotNull final ProjectDTO project = projectService.changeProjectStatusById(USER_ID, id, status);
        Assert.assertEquals(status, project.getStatus());
    }

}
