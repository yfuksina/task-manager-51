package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.fuksina.tm.command.AbstractCommand;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.dto.model.ProjectDTO;
import ru.tsc.fuksina.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("START DATE: " + DateUtil.toString(project.getDateStart()));
        System.out.println("END DATE: " + DateUtil.toString(project.getDateEnd()));
    }

}
