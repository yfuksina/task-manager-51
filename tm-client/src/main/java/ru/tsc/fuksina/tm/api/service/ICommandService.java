package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

}
