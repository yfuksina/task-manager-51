package ru.tsc.fuksina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().changeTaskStatusById(new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED));
    }

}
